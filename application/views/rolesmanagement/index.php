<?php
Calypso::AddPartialView('notices');
if(!empty($roles) && !empty($roleRef)) {
    Calypso::AddPartialView('_changeRoleModal',['roleRef'=>$roleRef]);
    $payload = [];
    $roleNameMap = [];

    foreach ($roles as $role) {
        if (!array_key_exists($role['Role']['id'], $payload)) {
            $payload[$role['Role']['id']] = [];
        }
        if (!array_key_exists($role['Role']['id'], $roleNameMap)) {
            $roleNameMap[$role['Role']['id']] = $role['Role']['name'];
        }
        array_push($payload[$role['Role']['id']], $role);

    }

//var_dump($payload);
//var_dump($roleNameMap);

    echo '<legend>Role Distribution</legend>';
    echo '<div class="row">';
    if (!empty($payload) && !empty($roleNameMap)) {
        foreach ($roleNameMap as $k => $name) {
            ?>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $name ?></h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">

                        <?php
                        if (!empty($payload[$k]) && is_array($payload[$k])) {
                            foreach ($payload[$k] as $role_user) {
                                ?>
                                <li class="list-group-item">
                                    <a data-toggle="modal" data-target="#change_role" data-id="<?php echo $role_user['RoleMap']['id'] ?>" data-sanwo_sid="<?php echo $role_user['User']['sanwo_id'] ?>" class="badge">Change</a>
                                    <strong><?php echo $role_user['User']['email'] ?></strong><br/>
                                    <?php echo $role_user['User']['phone'] ?><br/>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    echo '</div>';
}


?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#change_role").on('shown.bs.modal', function(event){
            var button = $(event.relatedTarget) // Button that triggered the modal
            $("#entity_id").val(button.data('id'));
        }).on('hidden.bs.modal', function(e){
            $("#entity_id").val('');
        });
    });
</script>
