<?php
Calypso::AddPartialView('_addStaffModal',['roles'=>$roles]);
Calypso::AddPartialView('notices');


?>


<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<legend>Staff Members</legend>
<button type="button" class="btn btn-success btn-xs pull-right evt-margin-top-x50-neg" data-toggle="modal" data-target="#add_staff">Add New</button><br/>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Staff List</h3>
    </div>
    <div class="panel-body">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Gender</th>
                <th>Registration Date</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $noProfileAgents = []; $csv_link = '';
            if(!empty($staff_members)) {
                $x = 0;
                foreach($staff_members as $staff_member){
                    if(!empty($staff_member['profile']) && !empty($staff_member['profile']['address'])) {
                        ?>
                        <tr>
                            <td><?php echo ++$x; ?></td>
                            <td><?php echo ucwords($staff_member['profile']['firstname']) . ' ' . ucwords($staff_member['profile']['middlename']) . ' ' . strtoupper($staff_member['profile']['lastname']) ?></td>
                            <td><?php echo $staff_member['email']; ?></td>
                            <td><?php echo $staff_member['telephone'] ?></td>
                            <td><?php echo $staff_member['profile']['gender'] == '1' ? 'M' : 'F'; ?></td>
                            <td><?php echo $staff_member['created_date'] ?></td>
                            <td><?php echo $staff_member['profile']['address'] ?></td>
                            <td>
                                <a class="btn btn-default btn-xs" href="<?php echo BASE_PATH; ?>/home/agentcollectionhistory/<?php echo $staff_member['id'] ?>/<?php echo base64_encode(ucwords($staff_member['profile']['firstname']) . ' ' . ucwords($staff_member['profile']['middlename']) . ' ' . strtoupper($staff_member['profile']['lastname'])); ?>">Detail</a>
                                <a class="btn btn-danger btn-xs" href="<?php echo BASE_PATH; ?>/home/agentcollectionhistory/<?php echo $staff_member['id'] ?>">Suspend</a>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
