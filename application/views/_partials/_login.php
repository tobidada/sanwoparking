<style type="text/css">.footer{display: none !important;}</style>
<div id="wrapper" style="position:absolute;">
    <div id="" class="gray-bg">
            <div class="container wrapper wrapper-content">
              <div class="col-md-6 col-md-offset-3">
              <?php
                    Calypso::AddPartialView('notices');
                    ?>
                <div class="form login">
            <div class="animated bounceIn">
                <img class="center " src="https://yewatoday.files.wordpress.com/2015/06/wpid-wp-1433348684137.png" align="middle">
                <h3>Welcome to FUNAABOT Transport Portal</h3>
            </div>
        <div class="page-content animated fadeInDown">

        <div class="row">
        <div class="space-6"></div>
        <div class="col-xs-10 col-xs-offset-1">
            <div id="login-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="center" style="color:#555;">
                            SanwoIQ Custom Portal
                        </h4>
                        <div class="ibox-content">
                            <form name="loginForm" id="loginForm" class="form-horizontal login" role="form"  method="post" enctype="multipart/form-data" action="<?php echo BASE_PATH; ?>/home/">
                                <div class="form-group">
                                    <div class="col-xs-10 col-xs-push-1">
                                       <span class="block input-icon input-icon-right">
                                       </i> <input type="text" class="form-control" placeholder="Email" name="email" id="inputEmail" value="" required focus autofocus="true"/>
                                            
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-10 col-xs-push-1">
                                       <span class="block input-icon input-icon-right">
                                            <input type="password" class="form-control" placeholder="Password" name="password" id="inputPassword" required/>
                                            
                                        </span>                                        
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <div class="col-xs-10 col-xs-push-1">
                                        <div class="todo-list small-list ui-sortable">
                                            <a href="#" class="check-link"><i class="fa fa-check-square"></i> </a>
                                            <span class="m-l-xs todo">Remember Me?</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="space"></div>
                                 <div class="clearfix"></div>
                                 <div class="row">
                                 <div class="col-xs-10 col-xs-push-1">
                                    <div class="col-xs-6 nopadl">
                                        <button type="submit" class="btn btn-primary">
                                            Login
                                        </button>
                                    </div>  
                                    <div class="col-xs-6 nopadr">
                                        <button type="reset" class="btn btn-default">
                                            Reset
                                        </button>
                                    </div>
                                </div> 
                                </div>       
                                <div class="form-group"> 
                                    <div class="col-xs-10 col-xs-push-1">                           
                                        <p class="m-t">
                                            <small>powered by Sanwo © 2015</small>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /widget-main -->

                    
                </div><!-- /widget-body -->
            </div><!-- /login-box -->

        </div><!-- /position-relative -->
        </div>
        </div><!-- /.page-content -->
        </div>
        </div> 
        <div class="footnote"><small>Sanwo Seamless Offline Payment Platform © 2015</small></div> 


