<?php
if(!empty($roleRef)) {
    ?>
    <div class="modal fade" id="change_role" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title">Change Role</h4>
                </div>
                <div class="modal-body">
                    <p>

                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                        <input id="entity_id" type="hidden" name="entity_id" value="">
                        <fieldset class="">
                            <div class="form-group">
                                <label for="textArea" class="control-label">Select Role</label>
                                <div class="">
                                    <?php
                                    echo HTML::makeSelectField('new_role','new_role',['Role'],$roleRef,'id','name',['form-control']);
                                    ?>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <br/>
                            <br/>

                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php
}