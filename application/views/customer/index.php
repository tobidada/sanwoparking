<?php
Calypso::AddPartialView('_addCustomerModal');
Calypso::AddPartialView('notices');


?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Customers
<a href="<?php echo BASE_PATH; ?>/home/users" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</h1>
<button type="button" class="btn btn-success btn-md pull-right evt-margin-top-x50-neg" data-toggle="modal" data-target="#add_customer">Add New Customer</button>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?> Customers</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
            <tr role="row">
                <th tabindex="0" rowspan="1" colspan="1">#</th>
                <th tabindex="0" rowspan="1" colspan="1">Name</th>
                <th tabindex="0" rowspan="1" colspan="1">Email</th>
                <th tabindex="0" rowspan="1" colspan="1">Phone</th>
                <!-- <th tabindex="0" rowspan="1" colspan="1">Gender</th> -->
                <th tabindex="0" rowspan="1" colspan="1">Reg. Date</th>
                <th tabindex="0" rowspan="1" colspan="1">Address</th>
                <th tabindex="0" rowspan="1" colspan="1">Action</th>
            </tr>
            </thead>
            <tbody>
        <?php
        if(!empty($customers)) {
            $x = 0;
            foreach ($customers as $customer) {

                ?>

                <tr>
                    <td><?php echo ++$x; ?></td>
                    <td><?php echo ucwords($customer['profile']['firstname']) . ' ' . ucwords($customer['profile']['middlename']) . ' ' . strtoupper($customer['profile']['lastname']) ?></td>
                    <td><?php echo $customer['email']; ?></td>
                    <td><?php echo $customer['telephone'] ?></td>
                    <!-- <td><?php echo $customer['profile']['gender'] == '1' ? 'M' : 'F'; ?></td> -->
                    <td><?php echo $customer['created_date'] ?></td>
                    <td><?php echo $customer['profile']['address'] ?></td>
                    <td>
                        <a  class="btn btn-primary btn-xs" href="<?php echo BASE_PATH; ?>/customer/transactions/<?php echo $customer['id'] ?>">Transactions</a>
                        <a class="btn btn-info btn-xs customer_detail" data-id="<?php echo $customer['id'] ?>" 

                         href="<?php echo BASE_PATH; ?>/customer/details/<?php echo $customer['id'] ?>"
                         >Detail</a>
                     <!--    <a  class="btn btn-success btn-xs" data-target="#add_customer_cycle" data-toggle="modal" href="#" data-id="<?php echo $customer['id'] ?>">Edit</a>
                        <a  class="btn btn-warning btn-xs" data-target="#add_saving_card" data-toggle="modal" data-name="<?php echo ucwords($customer['profile']['firstname']) . ' ' . ucwords($customer['profile']['middlename']) . ' ' . strtoupper($customer['profile']['lastname']) ?>" href="#" data-id="<?php echo $customer['id'] ?>">Attach Withdrawal Card</a>
                        <a class="btn btn-info btn-xs customer_detail" data-id="<?php echo $customer['id'] ?>" data-behaviour="ajax" data-toggle="modal" data-target="#customer_detail_modal" href="#/home/customerdetail/<?php echo $customer['id'] ?>/">Detail</a> -->
                    </td>
                </tr>
                <?php
            }
        }
        ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
<?php
Calypso::AddPartialView('_customerDetailModal');
Calypso::AddPartialView('_addSavingsCardModal');
Calypso::AddPartialView('_addCustomerCycleModal');
?>
<script type="text/javascript">
    var BASE_PATH = "<?php echo BASE_PATH; ?>";
    $(document).ready(function(){
        $("#customer_detail").unbind('click').on('click', function(){

        });
        $("#customer_detail_modal").on('shown.bs.modal', function(event){

            var button = $(event.relatedTarget) // Button that triggered the modal
            $.ajax({url: BASE_PATH+"/public/home/customerdetail/"+button.data('id'), success: function(result){
                $("#customer_detail_body").html(result);
            }});
        }).on('hidden.bs.modal', function(e){
            $("#customer_detail_body").html('Loading...');
        });


        $('table').DataTable();
    });

</script>
