<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/24/2015
 * Time: 12:17 AM
 */
use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;

use SanwoPHPAdapter\Globals\ServiceConstant;

class SettingController extends VanillaController{


	private $noAuth = ['index'];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index()
    {
    }

}