<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/26/2015
 * Time: 5:06 AM
 */

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;

class ManagementController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){}
    public function staff()
    {
        $postData = Calypso::getInstance()->post(true);
        $data = Calypso::getInstance()->session('user');
        if(!empty($postData))
        {
            if($this->validateData($postData,['email','telephone','address','firstname','lastname','gender','user_type_id'],false)){
                if(empty($postData['password']))
                {
                    $postData['password'] = BasicEncrypt::generatePassword();
                }
                $newStaff = new IssuerAdapter($data['id'], RequestHelper::getAccessToken());
                $resp = $newStaff->createIssuerUser($data['id'],$postData['password'],$postData['email'],$postData['telephone'],$postData['firstname'],$postData['lastname'],$postData['middlename'],$postData['address'],$postData['gender'],9);
                $respData = new ResponseHandler($resp);
                if($respData->getStatus() == ResponseHandler::STATUS_OK)
                {
                    $staff = new User();
                    $staff->sanwo_id = 1;
                    $staff->email = $postData['email'];
                    $staff->phone = $postData['telephone'];
                    $id = $staff->save();
                    if($id){
                        $rolemap = new RoleMap();
                        $rolemap->role_id = $postData['user_type_id'];
                        $rolemap->user_id = $id;
                        $rolemap->save();
                    }

                    Calypso::getInstance()->setFlashSuccessMsg('New Staff Member Added Successfully.');
                }else{
                    Calypso::getInstance()->setFlashErrorMsg('Ooops! could not add new staff member this time. #Reason:'.$respData->getError());
                }
            }else{
                Calypso::getInstance()->setFlashErrorMsg('Please fill up empty fields');
            }

        }
        $roles = new Role();
        $roles->where('status', 1);
        $roleData = $roles->search();

        $staffUsers = new UserAdapter($data['id'], RequestHelper::getAccessToken());
        $response = $staffUsers->getAllByType(9,0, 1000); //Issuer user
        $response = new ResponseHandler($response);
        $this->set('staff_members', $response->getData());
        $this->set('roles', $roleData);
    }
    public function contributionLifeCycle()
    {

    }
}