<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 11/1/2015
 * Time: 3:24 PM
 */
class RolesManagementController extends VanillaController
{


    public function index()
    {
        $postData = Calypso::getInstance()->post(true);
        if(!empty($postData))
        {
            if($this->validateData($postData,['entity_id','new_role'],false)){
                $newrolemap = new RoleMap();
                $newrolemap->id = $postData['entity_id'];
                $newrolemap->role_id = $postData['new_role'];
                $newrolemap->save();
                Calypso::getInstance()->setFlashSuccessMsg('Role updated successfully. Applicable in the next login');
            }else{
                Calypso::getInstance()->setFlashErrorMsg('Invalid request. Role not updated');
            }
        }

        $roleRef = new Role();
        $roleRef->where('status',1);
        $this->set('roleRef', $roleRef->search());

        $rolemap = new RoleMap();
        $rolemap->showHasOne();
        $rolemap->where('status',1);
        $roles = $rolemap->search();
        $this->set('roles', $roles);
        if(empty($roles)){
            Calypso::getInstance()->setFlashSuccessMsg('<h3>Ooops</h3>No user found here. You need to create a staff member to proceed here. Click the <strong>Staff Management</strong> menu to get started');
        }
    }
}