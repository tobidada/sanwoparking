<?php

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;

class CardController extends VanillaController{

    private $noAuth = ['index'];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }



    public function index(){
    	 $data = Calypso::getInstance()->session('user');

        // var_dump($data);
            $cardAdp = new CardAdapter($data['id'], RequestHelper::getAccessToken());
            $card_data = $cardAdp->getAll(0,200);
            $card_data = new ResponseHandler($card_data);
            if($card_data->getStatus() == ResponseHandler::STATUS_OK)
            {
                $cardData = $card_data->getData();

              //  var_dump($card_data);
                Calypso::getInstance()->session('cards', $cardData['data']);
                $this->set('cards', $cardData['data']);
            }
    }


    public function transactions($card_id){
    	$data = Calypso::getInstance()->session('user');
    	$transactions = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());
    	$transactions = $transactions->getCardTransactions($card_id, 50, 50);


    	//var_dump($transactions);
    	$transactions = new ResponseHandler($transactions);

    	//var_dump($transactions);
    }




    }


?>