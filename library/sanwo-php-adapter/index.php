<?php
/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 3:41 AM
 */

namespace SanwoPHPAdapter;

require_once "Globals/ServiceConstant.php";
require_once "Util/CurlAgent.php";
require_once "Util/Response.php";
require_once "Util/ResponseHandler.php";
require_once "Util/RequestHelper.php";
require_once "BaseAdapter.php";
require_once "UserAdapter.php";
require_once "TransactionAdapter.php";
require_once "TopupTransactionAdapter.php";
require_once "AgentAdapter.php";
require_once "DeviceAdapter.php";
require_once "CardAdapter.php";
require_once "CustomerAdapter.php";
require_once "IssuerAdapter.php";
require_once "CashierAdapter.php";
require_once "MerchantAdapter.php";
require_once "ReportAdapter.php";
require_once "Merchant_transactionsAdapter.php";
require_once "Merchant_syncAdapter.php";

//$adapter = new UserAdapter();
//var_dump($adapter->login("a@b.c", "123456"));