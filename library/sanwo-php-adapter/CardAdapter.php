<?php


namespace SanwoPHPAdapter;

use SanwoPHPAdapter\Globals\ServiceConstant;
class CardAdapter extends BaseAdapter {

    /**
     * @author Adegoke Obasa
     * @param $card_type_id
     * @param $issuer_id
     * @param $serial_number
     * @param $ref_number
     * @param $batch_number
     * @param $version
     * @param $metadata
     * @param $expiry_date
     * @return array|mixed|string
     */
    public function add($card_type_id, $issuer_id, $serial_number, $ref_number, $batch_number, $version, $metadata, $expiry_date)
    {
        return $this->request(
            "card/add/",
            array(
                'card_type_id' => $card_type_id,
                'issuer_id' => $issuer_id,
                'serial_number' => $serial_number,
                'ref_number' => $ref_number,
                'batch_number' => $batch_number,
                'version' => $version,
                'metadata' => $metadata,
                'expiry_date' => $expiry_date
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $card_id
     * @param $card_type_id
     * @param $issuer_id
     * @param $serial_number
     * @param $ref_number
     * @param $batch_number
     * @param $version
     * @param $metadata
     * @param $expiry_date
     * @return array|mixed|string
     */
    public function edit($card_id, $card_type_id, $issuer_id, $serial_number, $ref_number, $batch_number, $version, $metadata, $expiry_date)
    {
        return $this->request(
            "card/edit/",
            array(
                'card_id' => $card_id,
                'card_type_id' => $card_type_id,
                'issuer_id' => $issuer_id,
                'serial_number' => $serial_number,
                'ref_number' => $batch_number,
                'batch_number' => $version,
                'version' => $serial_number,
                'metadata' => $metadata,
                'expiry_date' => $expiry_date
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $card_id
     * @param $status
     * @return array|mixed|string
     */
    public function changeStatus($card_id, $status)
    {
        return $this->request(
            "card/changeStatus/",
            array(
                'card_id' => $card_id,
                'status' => $status,
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $card_id
     * @return array|mixed|string
     */
    public function get($card_id)
    {
        return $this->request(
            "card/get/",
            array(
                'card_id' => $card_id
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $serial_number
     * @return array|mixed|string
     */
    public function getBySerial($serial_number)
    {
        return $this->request(
            "card/getBySerial/",
            array(
                'serial_number' => $serial_number
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $ref_number
     * @return array|mixed|string
     */
    public function getByRef($ref_number)
    {
        return $this->request(
            "card/getByRef/",
            array(
                'ref_number' => $ref_number
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $offset
     * @param $count
     * @return array|mixed|string
     */
    public function getAll($offset = 0, $count = 1000)
    {
        return $this->request(
            "card/getAll/",
            array(
                'offset' => $offset,
                'count' => $count
            ),
            self::HTTP_GET
        );
    }
}