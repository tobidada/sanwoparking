<?php

require_once (ROOT . DS . 'config' . DS . 'config.php');
require_once (ROOT . DS . 'config' . DS . 'routing.php');
require_once (ROOT . DS . 'config' . DS . 'inflection.php');
require_once (ROOT . DS . 'library' . DS . 'global_config.php');
require_once (ROOT . DS . 'config' . DS . 'html.resource.php');
require_once (ROOT . DS . 'library' . DS . 'Calypso.class.php');
require_once (ROOT . DS . 'library' . DS . 'Calypso.mailer.class.php');
require_once (ROOT . DS . 'library' . DS . 'encrypt.php');
require_once (ROOT . DS . 'library' . DS . 'myphpcache'.DS.'index.php');
require_once (ROOT . DS . 'library' . DS . 'sanwo-php-adapter'.DS.'index.php');
require_once (ROOT . DS . 'library' . DS . 'shared.php');