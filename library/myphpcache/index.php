<?php

class MyPHPCache{
    public $cacheType = CacheTypes::file;
    public $identifier = 0;
    public function __construct(){
        try{
            if(!is_dir(CacheInternals::DIR)){
                mkdir(CacheInternals::DIR);
            }
            if(!is_dir(CacheInternals::DIR."/".CacheInternals::FILE_DIR)){
                mkdir(CacheInternals::DIR."/".CacheInternals::FILE_DIR);
            }
            if(!is_dir(CacheInternals::DIR."/".CacheInternals::DATA_DIR)){
                mkdir(CacheInternals::DIR."/".CacheInternals::DATA_DIR);
            }
            //$this->getCurrentPageCache($this->identifier);
        }catch(Exception $e){
            error_log(json_encode($e));
        }
    }
    public function isExpired($key,$type=CacheTypes::file){
        switch($type){
            case CacheTypes::file:
                if(!file_exists(CacheInternals::DIR."/".CacheInternals::FILE_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT)){
                    return true;
                }
                if(time() - CacheInternals::LIFE_SPAN >= filemtime(CacheInternals::DIR."/".CacheInternals::FILE_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT)){
                    return true;
                }
                return false;
                break;
            case CacheTypes::data:
                if(!file_exists(CacheInternals::DIR."/".CacheInternals::DATA_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT)){
                    return true;
                }
                if(time() - CacheInternals::LIFE_SPAN >= filemtime(CacheInternals::DIR."/".CacheInternals::DATA_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT)){
                    return true;
                }
                return false;
                break;
            case CacheTypes::object:
               /*
               Not yet implemented
               */
                return true;
                break;
        }

    }
    public function cacheData($key,$data,$type=CacheTypes::file){
        $dir = CacheInternals::DIR."/";
        switch($type){
            case CacheTypes::file:
                $dir.= CacheInternals::FILE_DIR;
                break;
            case CacheTypes::data:
                $dir.= CacheInternals::DATA_DIR;
                break;
            case CacheTypes::object:

                break;
        }
        $dir .= "/cache_".$key.".".CacheInternals::CACHE_EXT;
        $f = fopen($dir,"w");
        fwrite($f,$data);
        fclose($f);
    }
    public function startViewCache($identifier){
        try{
            $this->identifier = $identifier;
            //@ob_start('ob_gzhandler');
        }catch (Exception $e){ error_log(json_encode($e));}
    }
    public function endViewCache(){
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING'].$this->identifier;
        $this->cacheData(md5($url),ob_get_contents(),CacheTypes::file);
        ob_end_flush();
    }
    public function deleteCache($key){

    }
    public function touchCache($key){

    }
    public function getCurrentPageCache($identifer){
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING'].$identifer;
        $this->getCacheVersion(md5($url),CacheTypes::file);
    }
    public function getCacheVersion($key,$cacheType=CacheTypes::file){
        switch($cacheType){
            case CacheTypes::file:
                if(!$this->isExpired($key)){
                    //ob_start('ob_gzhandler');
                    readfile(CacheInternals::DIR."/".CacheInternals::FILE_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT);
                    echo 'Viewing cached page';
                    ob_end_flush();
                    die();
                }
                break;
            case CacheTypes::data:
                if(!$this->isExpired($key)){
                    //ob_start('ob_gzhandler');
                    echo file_get_contents(CacheInternals::DIR."/".CacheInternals::DATA_DIR."/cache_".$key.".".CacheInternals::CACHE_EXT);
                    ob_end_flush();
                    die();
                }

                break;
        }
    }

}

class CacheTypes {
    const file = "file";
    const data = "data";
    const object = "object";
}

class CacheInternals{
    const DIR = "../cache";
    const DATA_DIR = "data";
    const FILE_DIR = "views";
    const LIFE_SPAN = 3600;
    const CACHE_EXT = "cfg";
}
?>